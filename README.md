# Directori Privat pels snippets de Yasnippets

This is the default place where to store your private yasnippets.

This path will be loaded automatically and used whenever Yasnippets loads.

Ignored by spacemacs git repo
